
## fonctionnalités
une fonctionnalités qui permets de verifier la taille de l'image le postal le numéro de telephone et le mot de passe

```php
$errors = [];
if(!empty($_POST['submitted'])) {
//debug($_FILES);
// validation image
if($_FILES['image']['error'] > 0) {
if($_FILES['image']['error'] != 4) {
$errors['image'] = 'Error: ' . $_FILES['image']['error'];
} else {
$errors['image'] = 'Veuillez renseigner une image';
}
} else {
$file_name = $_FILES['image']['name'];
$file_size = $_FILES['image']['size'];
$file_tmp  = $_FILES['image']['tmp_name'];
$file_type = $_FILES['image']['type'];
// Taille du fichier
$sizeMax = 2000000; // 2mo
if($file_size > $sizeMax || filesize($file_tmp) > $sizeMax) {
$errors['image'] = 'Votre fichier est trop gros (max 2mo).';
} else {
// Type du fichier.
$allowedMimeType = array('image/png','image/jpeg','image/jpg');
$finfo = finfo_open(FILEINFO_MIME_TYPE);
$mime = finfo_file($finfo, $file_tmp);
if(!in_array($mime, $allowedMimeType)) {
$errors['image'] = 'Veuillez télécharger une image du type jpeg ou .png';
}
}
}

            if(count($errors) === 0) {
                // upload
                $point = strrpos($file_name, '.');
                $extension = substr($file_name,$point, strlen($file_name) - $point);
                $newfile = time() . '-' . generateRandomString(12);
                if(!is_dir('upload')) {
                    mkdir('upload');
                }
                if(move_uploaded_file($file_tmp, 'upload/' . $newfile.$extension)) {
                    $img = Image::make('upload/' . $newfile.$extension);
                    $img->fit(200, 200);
                    $img->save('upload/' . $newfile.'-200x200'.$extension);

                    $img2 = Image::make('upload/' . $newfile.$extension);
                    $img2->fit(500, 300);
                    $img2->save('upload/' . $newfile.'-500x300'.$extension);
                }
            }
        }
    }

  private function validate($v,$post)
    {
        $errors = [];
        $errors['email'] = $v->textValid($post['email'], 'email',5, 500);
        $errors['message'] = $v->textValid($post['message'], 'message',5, 20);
        $errors['password'] = $v->textValid($post['password'], 'password',5, 20);
        $errors['password2'] = $v->textValid($post['password2'], 'password2',5, 20);
        $errors['phone'] = $v->textValid($post['phone'], 'phone',10,11);
        $errors['postal'] = $v->textValid($post['postal'], 'postal',1,5);
        $errors['phone'] = $v->textValid($post['phone'], 'phone',10,11);
        return $errors;
    }
```


## conclusion

on n'a fait en sorte que chaque form avait une validation si il y avait un probléme ça afficherait erreur
