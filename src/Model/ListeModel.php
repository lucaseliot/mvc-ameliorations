<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ListeModel extends AbstractModel
{
    protected static $table = 'liste';

    protected $id;
    protected $title;
    protected $content;
    protected $status;

    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getcontent() {
        return $this->content;
    }
    public function getStatus() {
        return $this->status;
    }

    public static function insert($post) {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, description, status,) VALUES (?,?,?, NOW())",
            array($post['title'], $post['description'],$post['status'])
        );
    }

}