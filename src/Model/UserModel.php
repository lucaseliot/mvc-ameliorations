<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'user';

    public static function getUserByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " .self::getTable() . " WHERE email = ?",array($email),get_called_class(), true);
    }


    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (email, password, token, created_at) VALUES (?,?,?, NOW())",
            array($post['email'], $post['password_hash'], $post['token'])
        );
    }

    public static function addOneToLoginCount($id)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE" . self::$table . " SET count_login = count_login + 1 WHERE id = ?",
            array($id)
        );
    }

}