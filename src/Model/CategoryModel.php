<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CategoryModel extends AbstractModel
{
    protected static $table = 'category';

    protected $id;
    protected $title;
    protected $description;
    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }
    protected $created_at;
    protected $modified_at;

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, description, status, created_at) VALUES (?,?,?, NOW())",
            array($post['title'], $post['description'],$post['status'])
        );
    }


    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET title = ?, description = ?, status = ?, modified_at = NOW() WHERE id = ?",
            array($post['title'], $post['description'],$post['status'],$id)
        );
    }



}