<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class RecipeModel extends AbstractModel
{
    protected static $table = 'recette';

    protected $id;
    protected $title;
    protected $content;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }
    protected $super;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getSuper()
    {
        return mb_strtoupper($this->title);
    }


    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, content, cat_id) VALUES (?,?,?)",
            array($post['title'], $post['content'], $post['category'])
        );
    }


    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET title = ?, content = ?, cat_id = ? WHERE id = ?",
            array($post['title'], $post['content'], $post['category'],$id)
        );
    }


    public static function getAllRecipeOrderBy($column = 'title', $order ='ASC')
    {
        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
    }

}