<?php
namespace App\Service;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */
    public function phoneValid($phone) {
        $error = '';
        $phone_length = strlen($phone);
        if ($phone_length != 10) {
            $error = 'Le numéro de téléphone doit avoir 10 chiffres';
        }
        return $error;
    }
    /**
     * textValid
     * @param POST $phone string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */
    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }


    public function validPassword($password, $password2)
    {
        $error = '';
        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $error = 'Vos mots de passe sont différents';
            } elseif(mb_strlen($password) < 6) {
                $error = 'Votre mot de passe est trop court(min 6)';
            }
        } else {
            $error = 'Veuillez renseigner le mots de passe';
        }
        return $error;
    }

    public function validPhone($phone) {
        $error = '';
        if (!preg_match('/^[0-9]{10}$/', $phone)) {
            $error = 'Numéro de téléphone invalide.' .$phone .' voici un exemple: 0202020202';
            $phone = '0202020202';
            if ($error) {
                echo $error;
            } else {
                echo 'Numéro de téléphone valide';
            }
        }
        return $error;
    }

    public function validPostal($postal) {
        $error = '';
        if (!preg_match('/^[0-5]{5}/', $postal)) {
            $error = 'adresse postal est invalide.' .$postal . 'voici un exemple postal: 75750';

        }

        return $error;
    }

    public function image() {
        $errors = [];
        if(!empty($_POST['submitted'])) {
            // validation image
            if($_FILES['image']['error'] > 0) {
                if($_FILES['image']['error'] != 4) {
                    $errors['image'] = 'Error: ' . $_FILES['image']['error'];
                } else {
                    $errors['image'] = 'Veuillez renseigner une image';
                }
            } else {
                $file_name = $_FILES['image']['name'];
                $file_size = $_FILES['image']['size'];
                $file_tmp  = $_FILES['image']['tmp_name'];
                $file_type = $_FILES['image']['type'];
                // Taille du fichier
                $sizeMax = 3000000; // 3mo
                if($file_size > $sizeMax || filesize($file_tmp) > $sizeMax) {
                    $errors['image'] = 'Votre fichier est trop gros (max 3mo).';
                } else {
                    // Type du fichier.
                    $allowedMimeType = array('image/png','image/jpeg','image/jpg');
                    $finfo = finfo_open(FILEINFO_MIME_TYPE);
                    $mime = finfo_file($finfo, $file_tmp);
                    if(!in_array($mime, $allowedMimeType)) {
                        $errors['image'] = 'Veuillez télécharger une image du type jpeg ou .png';
                    }
                }
            }

            if(count($errors) === 0) {
                // upload
                $point = strrpos($file_name, '.');
                $extension = substr($file_name,$point, strlen($file_name) - $point);
                $newfile = time() . '-' . generateRandomString(12);
                $newfile = date('Y_m_d_H_i_s') . '-' . generateRandomString(3) . $extension;
                if(!is_dir('upload')) {
                    mkdir('upload');
                }
                if(move_uploaded_file($file_tmp, 'upload/' . $newfile.$extension)) {
                    $img = Image::make('upload/' . $newfile.$extension);
                    $img->fit(200, 200);
                    $img->save('upload/' . $newfile.'-200x200'.$extension);

                    $img2 = Image::make('upload/' . $newfile.$extension);
                    $img2->fit(500, 300);
                    $img2->save('upload/' . $newfile.'-500x300'.$extension);
                }
            }
        }
    }
}