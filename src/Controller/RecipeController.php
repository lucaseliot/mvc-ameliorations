<?php

namespace App\Controller;

use App\Model\CategoryModel;
use App\Model\RecipeModel;
use App\Service\Form;
use App\Service\Validation;
use JasonGrimes\Paginator;

class RecipeController extends BaseController
{
    public function index() {
        $currentPage = 1;
        $offset = 0;
        if(!empty($_GET['page'])) {
            $currentPage = $_GET['page'];

        }

        $totalItems = 100;
        $itemsPerPage = 10;

        $urlPattern = '/les-recettes?page=(:num)';
        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $this->addLog('Bon appetit');
        $this->render('app.recipe.index', array(
//            'recettes' => RecipeModel::all()
            'recettes' => RecipeModel::getAllRecipeOrderBy(),
            'burgerjs' => 'yes',
            'paginator' => $paginator
        ), 'admin');
    }

    public function single($id) {
        $recette = $this->getRecipeByIdOr404($id);
        $category = CategoryModel::findById($recette->cat_id);
        $this->render('app.recipe.single',array(
            'recette' => $recette,
            'category' => $category,
        ), 'admin');
    }

    public function add() {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                RecipeModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Merci pour votre nouvelle recette!');
                // redirection
                $this->redirect('les-recettes');
            }
        }
        $form = new Form($errors);
        $this->render('app.recipe.add', array(
            'form' => $form,
            'categories' => CategoryModel::all()
        ),'admin');
    }

    public function edit($id)
    {
        $recette = $this->getRecipeByIdOr404($id);
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                RecipeModel::update($id,$post);
                // Message flash
                $this->addFlash('success', 'Merci pour l\'édition de cette recette!');
                // redirection
                $this->redirect('les-recettes');
            }
        }
        $form = new Form($errors);
        $this->render('app.recipe.edit', array(
            'form' => $form,
            'recette' => $recette,
            'categories' => CategoryModel::all()
        ), 'admin');
    }
    // delete
    public function delete($id) {
        $this->getRecipeByIdOr404($id);
        RecipeModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé cette recette!');
        $this->redirect('les-recettes');
    }

    private function getRecipeByIdOr404($id)
    {
        $recette = RecipeModel::findById($id);
        if(empty($recette)) {
            $this->Abort404();
        }
        return $recette;
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'titre',2, 100);
        $errors['content'] = $v->textValid($post['content'], 'contenu',5, 500);
        $verifCat = CategoryModel::findById($post['category']);
        if(empty($verifCat)) {
            $errors['category'] = 'Error';
        }
        return $errors;
    }
}