<?php

namespace App\Controller;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\Config;

/**
 *
 */
class DefaultController extends BaseController
{

    private $v;

    public function __construct()
    {
        $this->v = new Validation();
    }

    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);

        $fruits = array('Banane','Kiwi','Papaye');
        $this->render('app.default.frontpage',array(
            'message' => $message,
            'dede' => 'dede',
            'legumes' => $fruits
        ));
    }




    public function image() {
        $errors = [];
        if(!empty($_POST['submitted'])) {
            // validation image
            if($_FILES['image']['error'] > 0) {
                if($_FILES['image']['error'] != 4) {
                    $errors['image'] = 'Error: ' . $_FILES['image']['error'];
                } else {
                    $errors['image'] = 'Veuillez renseigner une image';
                }
            } else {
                $file_name = $_FILES['image']['name'];
                $file_size = $_FILES['image']['size'];
                $file_tmp  = $_FILES['image']['tmp_name'];
                $file_type = $_FILES['image']['type'];
                // Taille du fichier
                $sizeMax = 3000000; // 3mo
                if($file_size > $sizeMax || filesize($file_tmp) > $sizeMax) {
                    $errors['image'] = 'Votre fichier est trop gros (max 3mo).';
                } else {
                    // Type du fichier.
                    $allowedMimeType = array('image/png','image/jpeg','image/jpg');
                    $finfo = finfo_open(FILEINFO_MIME_TYPE);
                    $mime = finfo_file($finfo, $file_tmp);
                    if(!in_array($mime, $allowedMimeType)) {
                        $errors['image'] = 'Veuillez télécharger une image du type jpeg ou .png';
                    }
                }
            }

            if(count($errors) === 0) {
                // upload
                $point = strrpos($file_name, '.');
                $extension = substr($file_name,$point, strlen($file_name) - $point);
                $newfile = time() . '-' . generateRandomString(12);
                $newfile = date('Y_m_d_H_i_s') . '-' . generateRandomString(3) . $extension;
                if(!is_dir('upload')) {
                    mkdir('upload');
                }
                if(move_uploaded_file($file_tmp, 'upload/' . $newfile.$extension)) {
                    $img = Image::make('upload/' . $newfile.$extension);
                    $img->fit(200, 200);
                    $img->save('upload/' . $newfile.'-200x200'.$extension);

                    $img2 = Image::make('upload/' . $newfile.$extension);
                    $img2->fit(500, 300);
                    $img2->save('upload/' . $newfile.'-500x300'.$extension);
                }
            }
        }
    }//
    public function contact() {
        $errors = array();
        if(!empty($_POST['submitted'])) {

            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                DefaultController::insert($post);
                $this->addFlash('success', 'Merci pour votre message!');
                $this->redirect('contacts');
            }
        }
        $form = new Form($errors);
        $this->render('app.default.contact', array(
            'form' => $form,
        ));
    }

    public function mention() {
        $this->render('app.default.mention');
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }

    private function validate($v,$post,)
    {
        $errors = [];
        $errors['pseudo'] = $v->textValid($post['pseudo'], 'pseudo',5, 40);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 20);
        $errors['message'] = $v->textValid($post['message'], 'message',5, 500);
        $errors['password'] = $v->validPassword($post['password'], 'mot de passe',5, 20);
        $errors['password2'] = $v->validPassword($post['password2'], ' mot de passe',5, 20);
        $errors['postal'] = $v->validPostal($post['postal'], 'postal');
        $errors['phone'] = $v->validPhone($post['phone'], 'numero');
        $errors['ville'] = $v->textValid($post['ville'], 'ville',10,10);
        $errors['adresse'] = $v->textValid($post['adresse'], 'adresse',10,11);
        return $errors;
    }
}
