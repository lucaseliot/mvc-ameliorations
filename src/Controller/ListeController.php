<?php

namespace App\Controller;


use App\Model\ListeModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\Config;


class ListeController extends BaseController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->statusList = (new Config())->get('listStatus');
        $this->v = new Validation();
    }


    public function index() {
        $this->render('app.liste.index', array(
            'liste' => ListeModel::all()
        ), 'admin');
    }

    public function single($id) {
        $liste = $this->getlisteByIdOr404($id);
        $this->render('app.liste.single', array(
            'liste' => $liste,
        ), 'liste');
    }

    public function add() {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->validate($this->v,$post);
            ListeModel::insert($post);
            $this->addFlash('succes', 'merci pour votre nouvelle liste');
            $this->redirect('les-listes');
        }
        $form = new Form($errors);
        $this->render('app.liste.add', array(
            'form' => $form,
            'statusList' => $this->statusList
        ), 'admin');

    }

    public function edit($id) {
        $liste = $this->getlisteByIdOr404($id);
        $errors = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v, $post);
            if ($v->IsValid($errors)) {
                ListeModel::update($id,$post);
            }
        }
        $form = new Form($errors);
        $this->render('app.liste.edit', array(
            'form' => $form,
            'statusList' => $this->statusList
        ), 'admin');
    }


}