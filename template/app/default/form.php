<form action ='' method="post" novalidate>

    <?php echo $form->label('pseudo', 'pseudo') ?>
    <?php echo $form->input('pseudo', 'pseudo', $contact->pseudo ?? '') ?>
    <?php echo $form->error('pseudo',) ?>

    <?php echo $form->label('email', 'email') ?>
    <?php echo $form->input('email', 'email', $contact->email ?? '') ?>
    <?php echo $form->error('email',) ?>

    <?php echo $form->label('password', 'password') ?>
    <?php echo $form->input('password', 'password' , $contact->password ?? '') ?>
    <?php echo $form->error('password',) ?>

    <?php echo $form->label('password2', 'password2') ?>
    <?php echo $form->input('password2', 'password' , $contact->password2 ?? '') ?>
    <?php echo $form->error('password2',) ?>

    <?php echo $form->label('message', 'message') ?>
    <?php echo $form->textarea('message', 'message' , $contact->message ?? '') ?>
    <?php echo $form->error('message',) ?>

    <?php echo $form->label('phone', 'phone') ?>
    <?php echo $form->input('phone', 'text', $contact->phone ?? '') ?>
    <?php echo $form->error('phone') ?>

    <?php echo $form->label('postal', 'postal') ?>
    <?php echo $form->input('postal', 'postal', $contact->postal ?? '' ) ?>
    <?php echo $form->error('postal') ?>

    <?php echo $form->label('ville', 'ville') ?>
    <?php echo $form->input('ville', 'ville', $contact->ville ?? '') ?>
    <?php echo $form->error('ville') ?>

    <?php echo $form->label('adresse', 'adresse') ?>
    <?php echo $form->input('adresse', 'adresse', $contact->adresse ?? '') ?>
    <?php echo $form->error('adresse') ?>


    <?php echo $form->label('image', 'image') ?>
    <?php echo $form->input('image', 'file'), $contact->file ?? '' ?>
    <?php echo $form->error('image', 'file') ?>

    <?php echo $form->submit('submitted', $textButton); ?>

</form>