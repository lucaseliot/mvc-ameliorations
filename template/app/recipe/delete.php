<h2>delete une recette </h2>

<form action="" method="post" novalidate>
    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title', 'text', $recette->title); ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('content'); ?>
    <?php echo $form->textarea('content', $recette->content); ?>
    <?php echo $form->error('content'); ?>

    <?php echo $form->submit('submitted', 'delete'); ?>
</form>
