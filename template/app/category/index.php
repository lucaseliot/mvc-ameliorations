<h1>Mes Catégories</h1>

<p><a class="btn" href="<?php echo $view->path('add-category'); ?>">Ajouter une category</a></p>

<?php foreach ($categorys as $category) {
    echo '<div><h2>'.$category->title.'</h2>
        <a class="btn" href="' .$view->path('category', array('id' => $category->id)).'">Détails</a>
        <a class="btn" href="' .$view->path('edit-category', array('id' => $category->id)).'">Editer</a>
        <a class="btn" onclick="return confirm(\'Voulez-vous effacer ?\')" href="' .$view->path('delete-category', array('id' => $category->id)).'">Effacer</a>
</div>';
}