<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title','text', $category->title ?? '') ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('description'); ?>
    <?php echo $form->textarea('description', $category->description ?? ''); ?>
    <?php echo $form->error('description'); ?>

    <?php echo $form->label('status'); ?>
    <?php echo $form->selectPerso('status',$statusList, $category->status ?? ''); ?>
    <?php echo $form->error('status'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>