<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('app'); ?>
  </head>
  <body>

    <header id="masthead">
      <nav>
          <ul>
              <li><a href="<?= $view->path(''); ?>">Home</a></li>
              <?php if($view->isLogged()) { ?>
                  <li><a href="<?= $view->path('logout'); ?>">Deconnexion</a></li>
              <?php } else { ?>
                  <li><a href="<?= $view->path('register'); ?>">Inscription</a></li>
                  <li><a href="<?= $view->path('login'); ?>">Connexion</a></li>
              <?php } ?>
              <li><a href="<?= $view->path('contact'); ?>">Contact</a></li>
              <li><a href="<?= $view->path('mentions-legales'); ?>">Mentions légales</a></li>
          </ul>
      </nav>
    </header>
  <?php
  foreach ($view->getFlash() as $flash) {
      echo '<p class="'.$flash['type'].'">'.$flash['message'].'</p>';
  }
  ?>

    <div class="container">
        <?= $content; ?>
    </div>

    <footer id="colophon">
        <div class="wrap">
            <p>MVC 6 - Framework Pédagogique.</p>
        </div>
    </footer>
  <?php echo $view->add_webpack_script('app'); ?>
  </body>
</html>
